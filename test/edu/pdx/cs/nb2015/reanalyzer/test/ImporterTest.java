package edu.pdx.cs.nb2015.reanalyzer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import edu.pdx.cs.nb2015.reanalyzer.RmlsRecord;
import edu.pdx.cs.nb2015.reanalyzer.Constants;
import edu.pdx.cs.nb2015.reanalyzer.DBManager;
import edu.pdx.cs.nb2015.reanalyzer.Importer;
import edu.pdx.cs.nb2015.reanalyzer.Utils;

public class ImporterTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testFetchRmlsData() throws URISyntaxException,
            UnsupportedEncodingException {
        Response resp = Importer.sendFirstRequestToRmls("97201", "", "", "");
        assertEquals(302, resp.getStatus());

        Map<String, NewCookie> cookies = resp.getCookies();
        URI newLocation = new URI(Constants.RMLS_START_PAGE).resolve(resp
                .getLocation());

        assertNotNull(cookies);
        assertNotNull(newLocation);

        String path = newLocation.getPath();
        Map<String, String> params = Utils.splitQuery(newLocation.getQuery());
        resp = Importer
                .sendOneSubsequentRequestToRmls(path, params, cookies, 1);
        assertEquals(200, resp.getStatus());
    }

    @Test
    public void testParseRmlsData() throws IOException {
        // TODO test duplicate records
        File file = new File("testdata/RMLS_page.html");
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        Set<RmlsRecord> actual = Importer.parseRmlsData(new String(data,
                "UTF-8"));
        Set<RmlsRecord> expected = new HashSet<RmlsRecord>();
        expected.add(new RmlsRecord("1205 SW CARDINELL DR  #405 Portland, OR",
                97201, 159950));
        expected.add(new RmlsRecord("1104 SW COLUMBIA ST Portland, OR", 97201,
                215000));
        expected.add(new RmlsRecord("855 SW BROADWAY DR  #23 Portland, OR",
                97201, 245000));
        expected.add(new RmlsRecord("255 SW HARRISON ST Portland, OR", 97201,
                248000));
        expected.add(new RmlsRecord("111 SW HARRISON ST  #19A Portland, OR",
                97201, 249900));

        assertTrue(expected.equals(actual));
    }

    @Test
    public void testZillowAPIGetZestimate() {
        Client client = ClientBuilder.newClient(new ClientConfig());
        String entity = client.target("http://www.zillow.com/webservice/")
                .path("GetZestimate.htm").queryParam("zpid", "53956393")
                .queryParam("zws-id", Constants.ZWS_ID)
                .request(MediaType.TEXT_PLAIN_TYPE).get(String.class);
        assertTrue(entity
                .startsWith("<?xml version=\"1.0\" encoding=\"utf-8\"?><Zestimate"));
    }

    @Test
    public void testGetZillowDeepSearchResults()
            throws UnsupportedEncodingException {

        Set<RmlsRecord> rmlsRecords = new HashSet<RmlsRecord>();
        RmlsRecord rr = new RmlsRecord("111 SW HARRISON ST  #19A Portland, OR",
                97201, 100000);
        rmlsRecords.add(rr);
        Map<RmlsRecord, String> result = Importer
                .getZillowDeepSearchResults(rmlsRecords);
        assertEquals(1, result.size());
        assertTrue(result
                .get(rr)
                .startsWith(
                        "<?xml version=\"1.0\" encoding=\"utf-8\"?><SearchResults:searchresults"));
    }

    @Test
    public void testParseZillowDeepSearchResults()
            throws ParserConfigurationException, SAXException, IOException {
        String resp = "<?xml version=\"1.0\" encoding=\"utf-8\"?><SearchResults:searchresults xsi:schemaLocation=\"http://www.zillow.com/static/xsd/SearchResults.xsd http://www.zillowstatic.com/vstatic/9b53252/static/xsd/SearchResults.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SearchResults=\"http://www.zillow.com/static/xsd/SearchResults.xsd\"><request><address>1205+SW+CARDINELL+DR++#405+Portland,+OR</address><citystatezip>97201</citystatezip></request><message><text>Request successfully processed</text><code>0</code></message><response><results><result><zpid>2117885668</zpid><links><homedetails>http://www.zillow.com/homedetails/1205-SW-Cardinell-Dr-UNIT-405-Portland-OR-97201/2117885668_zpid/</homedetails><mapthishome>http://www.zillow.com/homes/2117885668_zpid/</mapthishome><comparables>http://www.zillow.com/homes/comps/2117885668_zpid/</comparables></links><address><street>1205 SW Cardinell Dr UNIT 405</street><zipcode>97201</zipcode><city>Portland</city><state>OR</state><latitude>45.510189</latitude><longitude>-122.688888</longitude></address><FIPScounty></FIPScounty><useCode>Condominium</useCode><yearBuilt>1951</yearBuilt><lotSizeSqFt>435</lotSizeSqFt><finishedSqFt>598</finishedSqFt><bathrooms>1.0</bathrooms><bedrooms>0</bedrooms><lastSoldDate>03/20/2015</lastSoldDate><lastSoldPrice currency=\"USD\">141000</lastSoldPrice><zestimate><amount currency=\"USD\">278160</amount><last-updated>02/16/2016</last-updated><oneWeekChange deprecated=\"true\"></oneWeekChange><valueChange duration=\"30\" currency=\"USD\">-12001</valueChange><valuationRange><low currency=\"USD\">203057</low><high currency=\"USD\">339355</high></valuationRange><percentile>0</percentile></zestimate><localRealEstate><region name=\"Goose Hollow\" id=\"403526\" type=\"neighborhood\"><links><overview>http://www.zillow.com/local-info/OR-Portland/Goose-Hollow/r_403526/</overview><forSaleByOwner>http://www.zillow.com/goose-hollow-portland-or/fsbo/</forSaleByOwner><forSale>http://www.zillow.com/goose-hollow-portland-or/</forSale></links></region></localRealEstate></result></results></response></SearchResults:searchresults><!-- H:002  T:21ms  S:1053  R:Wed Feb 17 20:30:25 PST 2016  B:4.0.25234-master.df01d52~hotfix_pre.361b6ff -->";
        Map<RmlsRecord, String> m = new HashMap<RmlsRecord, String>();
        m.put(new RmlsRecord("1205 SW Cardinell Dr UNIT 405", 97201, 123456),
                resp);
        List<Map<String, String>> lst = Importer
                .parseZillowDeepSearchResults(m);
        assertEquals(1, lst.size());
        Map<String, String> map = lst.get(0);
        assertNotNull(map);
        assertEquals("2117885668", map.get("zpid"));
        assertEquals(
                "http://www.zillow.com/homedetails/1205-SW-Cardinell-Dr-UNIT-405-Portland-OR-97201/2117885668_zpid/",
                map.get("homedetails"));
        assertEquals("http://www.zillow.com/homes/2117885668_zpid/",
                map.get("mapthishome"));
        assertEquals("http://www.zillow.com/homes/comps/2117885668_zpid/",
                map.get("comparables"));
        assertEquals("1205 SW Cardinell Dr UNIT 405", map.get("street"));
        assertEquals("97201", map.get("zipcode"));
        assertEquals("Portland", map.get("city"));
        assertEquals("OR", map.get("state"));
        assertEquals("45.510189", map.get("latitude"));
        assertEquals("-122.688888", map.get("longitude"));
        assertNull(map.get("FIPScounty"));
        assertEquals("Condominium", map.get("useCode"));
        assertEquals("1951", map.get("yearBuilt"));
        assertEquals("435", map.get("lotSizeSqFt"));
        assertEquals("598", map.get("finishedSqFt"));
        assertEquals("1.0", map.get("bathrooms"));
        assertEquals("0", map.get("bedrooms"));
        assertEquals("03/20/2015", map.get("lastSoldDate"));
        assertEquals("278160", map.get("zestimate"));
        assertEquals("Goose Hollow", map.get("region_name"));
        assertEquals("403526", map.get("region_id"));
        assertEquals("neighborhood", map.get("region_type"));
        assertEquals("123456", map.get("rmls_price"));
    }

    @Test
    public void testDBManager() throws ClassNotFoundException {
        Connection conn = null;
        try {
            conn = DBManager.getConnection();
            Statement statement = conn.createStatement();

            statement.executeUpdate("drop table if exists person");
            statement
                    .executeUpdate("create table person (id integer, name string)");
            statement.executeUpdate("insert into person values(1, 'leo')");
            statement.executeUpdate("insert into person values(2, 'yui')");
            ResultSet rs = statement
                    .executeQuery("select * from person order by id");
            rs.next();
            assertEquals("leo", rs.getString("name"));
            assertEquals(1, rs.getInt("id"));
            rs.next();
            assertEquals("yui", rs.getString("name"));
            assertEquals(2, rs.getInt("id"));
            statement.executeUpdate("drop table person");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
}
