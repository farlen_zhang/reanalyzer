package edu.pdx.cs.nb2015.reanalyzer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import edu.pdx.cs.nb2015.reanalyzer.Utils;

public class UtilsTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testJoin() {
        String[] a = new String[] { "foo", "bar", "baz" };
        assertEquals("foo||bar||baz", Utils.join("||", a));
        assertNull(Utils.join(null, a));
    }
}
