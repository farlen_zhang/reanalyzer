//<SCRIPT>
/*
'********************************************************************* 
' Name:  inc_report.js
' Class:  jsRERPOT_
' Created date: 5/19/2003 
' Created by:   Keiji Ikuta
' Purpose:      JavaScript Library for GenReport
' Inputs:
' Outputs:
' Notes:     
'********************************************************************* 
*/

var GenReport_iSpecificOrderCount;
GenReport_iSpecificOrderCount=1;

var GenReport_SearchFormURL;
var GenReport_ResultsReportId;
var GenReport_SearchFormQueryVal;
GenReport_SearchFormQueryVal = "";

function GenReport_Load_OrderBy(strKey,strReportID)
{
	var curOBK=document.mainform.OBK.value;
	document.mainform.OBK.value=strKey;
	if(curOBK!=strKey){
		document.mainform.OBD.value='ASC'; // Initial order is 'ASC'
	}
	else{
		if(document.mainform.OBD.value=='ASC'){
			document.mainform.OBD.value='DESC';
		}
		else{
			document.mainform.OBD.value='ASC';
		}
	}
	document.mainform.NO.value=1;
	document.mainform.RID.value=strReportID;
	document.mainform.ID.value='';
	document.mainform.P.value=1;
	document.mainform.submit();
}

function GenReport_Load_FullReport(MLID,strReportID)
{
	document.mainform.RID.value=strReportID;
	document.mainform.ID.value=MLID;
	document.mainform.submit();
}

function GenReport_GoBacktoResult()
{
	document.mainform.RID.value=GenReport_ResultsReportId;
	document.mainform.ID.value='';
	document.mainform.submit();
}
/*
'********************************************************************* 
' Name:  GenReport_GoBacktoForm
' Created date: 5/19/2003 
' Created by:   Keiji Ikuta
' Purpose:      navigate back to calling search Form
' Inputs:
' Outputs:
' Notes:     
' Update date: 6/2/2010
' Update by: Tim French
' Notes: RCOM-52 added State ID from QueryString ST_ID
'********************************************************************* 
*/
function GenReport_GoBacktoForm() {
    // begin: RCOM-52 (TRF) Build if search string not empty
   if (GenReport_SearchFormQueryVal != "") {
    window.location = (GenReport_SearchFormURL + "?ID=" + GenReport_SearchFormQueryVal);
   }
   else{
      window.location = GenReport_SearchFormURL;
  }
   // end: RCOM-52
}

function GenReport_GoToPage(iPage,strReportID)
{
	document.mainform.RID.value=strReportID;
	document.mainform.ID.value='';
	document.mainform.P.value = iPage
	document.mainform.submit();
}

function GenReport_SetPrint(bPrintMode)
{
	var v=true;
	if(bPrintMode) v=false;
	
	//NONPRINTSECTION_?
	var i;
	var e;
	var divs=document.all.tags("DIV");
	var s;
	
	for(i=0;i<divs.length;i++){
		e=divs(i);
		s=e.id;
		if(s.substr(0,15)=="NONPRINTSECTION"){
			//alert("["+s+"]");
			if(v==true){
				e.style.display='block';
			}
			else{
				e.style.display='none';
			}
		}
	}
	
	if(v){

	}
	else{
		MainScreen_Outer.style.backgroundColor='#FFFFFF';
		MainScreen_Outer.style.border='none';
		MainScreen_InnerShade.style.backgroundColor='#FFFFFF';
		MainScreen_InnerShade.style.border='none';
		MainScreen_Inner.style.backgroundColor='#FFFFFF';
		MainScreen_Inner.style.borderTop='none';
		MainScreen_Inner.style.borderLeft='none';
		MainScreen_Inner.style.borderBottom='none';
		MainScreen_Inner.style.borderRight='none';
	}
	
}

function GenReport_OnPageClick()
{
	//alert('GenReport_OnPageClick');
	GenReport_SetPrint(false);
}

function GenReport_PrintPage()
{
	try
	{
		window.print();
	}
	catch(e)
	{ 
	}
}

function xVer()
{
	if (!bPrintMode)
	{
		var cbm, cbml;
		cbm = document.getElementById("cbm");
		if (cbm == null)
			cbml = 0;
		else
			cbml = cbm.innerHTML.length;

		if (cbml < 1500)
			window.location = "/RC2/UI/expired.asp";
	}
}

function GenReport_OpenPrintPreview(strURL)
{
	//window.open(strURL,"","width=780,height=500,resizable=1,scrollbars=1,status=1");
	window.open(strURL,"","width=780,height=500,resizable=1,scrollbars=1,status=1,menubar=1");
}
