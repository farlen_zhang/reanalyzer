/*
'********************************************************************* 
' Name:  inc_progressmsg.js
' Created date: 5/21/2003 
' Created by:   Keiji Ikuta
' Purpose:      Display progress message.
' Inputs:
' Outputs:
' Notes:     
'   If the screen changed, the dialogbox will be automatically closed.
' Update date:
' Update by:
'********************************************************************* 
*/

var ProgressMsgURL=G_V2_BaseURL+"/system/ProgressMsg.asp";
var ProgressMsgWin=null;
//
function ProgressMsg_Show(msg)
{
	//window.status=msg;
	
	var theWidth=400;
	var theHeight=250; 
	var ScreenWidth = window.screen.width;
	var ScreenHeight = window.screen.height;
	var theLeft = (ScreenWidth-theWidth)/2;
	var theTop = (ScreenHeight-theHeight)/2;

	var newurl;
	newurl=ProgressMsgURL;
	if(arguments.length>=1)
	{
		if(msg!=""){
			newurl=newurl + "?strMessage=" + msg
		}
	}
	ProgressMsgWin = showModelessDialog(newurl,"ProgressMsgWindow","scroll:off;center:Yes;status:no;help:no;dialogWidth:" +  theWidth +"px;dialogHeight:" + theHeight + "px");
	//ProgressMsgWin=window.open("","ProgressMsgWindow","dependent, alwaysRaised, toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resizable=0,top="+theTop+",left="+theLeft+",width="+theWidth+",height="+theHeight);
	//ProgressMsgWin=window.open("","ProgressMsgWindow","dependent, alwaysRaised, toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resizable=0,width="+theWidth+",height="+theHeight);
	
	//var waitcount=0;
	//while(!ProgressMsgWin){
	//	waitcount=waitcount+1;
	//	if(waitcount>100) break;
	//}
	//alert("Show:"+typeof(ProgressMsgWin));
	
	//ProgressMsgWin.focus();
}

/*
'********************************************************************* 
' Name:  ProgressMsg_Hide
' Created date: 11/1/2002 
' Created by:   Keiji Ikuta
' Purpose:      Task#166
'               Close progress message.
' Inputs:
' Outputs:
' Notes:     
' Update date:  3/17/2003
' Update by:    Keiji Ikuta
' Update Note:  Accessing ProgressMsgWin.isclosed cause RPC failer.
'                
'********************************************************************* 
*/

function ProgressMsg_Hide_Ex(blnClosedCheck)
{
	try{// To avoid "RPC failer" on Win98. (3/23/2003 Keiji)
		if(!ProgressMsgWin){
		}
		else{
			if(typeof(ProgressMsgWin)=="object"){
				
				//This isn't the cause. Unknown yet. (3/31/2003 Keiji)
				//if(blnClosedCheck==true){
					// This cause "RPC failer" on Win98. Don't know why??? (3/23/2003 Keiji)
					//if(!ProgressMsgWin.isclosed){ 
					//	ProgressMsgWin.HideWindow(); //close();
					//}
				//}
				
				ProgressMsgWin.close();  
			}
		}
	}
	catch(error){
	}
	
	ProgressMsgWin=null;
}


function ProgressMsg_Hide()
{
	ProgressMsg_Hide_Ex(true);
}


