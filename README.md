The easiest way to run this is using ant:

    $ ant

If you don't have ant, you can build and run this manually

    $ mkdir bin
    $ javac -cp "lib/*:lib/jaxrs-ri/lib/*:lib/jaxrs-ri/api/*:lib/jaxrs-ri/ext/*:src" src/edu/pdx/cs/nb2015/reanalyzer/*.java -d bin
    $ javac -cp "lib/*:lib/jaxrs-ri/lib/*:lib/jaxrs-ri/api/*:lib/jaxrs-ri/ext/*:src:test:/usr/share/java/junit.jar" test/edu/pdx/cs/nb2015/reanalyzer/test/*.java -d bin

    $ java -cp "lib/*:lib/jaxrs-ri/lib/*:lib/jaxrs-ri/api/*:lib/jaxrs-ri/ext/*:bin" edu.pdx.cs.nb2015.reanalyzer.UI
    $ java -cp "lib/*:lib/jaxrs-ri/lib/*:lib/jaxrs-ri/api/*:lib/jaxrs-ri/ext/*:bin" edu.pdx.cs.nb2015.reanalyzer.Importer
    $ java -cp "lib/*:lib/jaxrs-ri/lib/*:lib/jaxrs-ri/api/*:lib/jaxrs-ri/ext/*:bin" edu.pdx.cs.nb2015.reanalyzer.DBManager


