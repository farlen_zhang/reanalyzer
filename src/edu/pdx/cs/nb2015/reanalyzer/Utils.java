package edu.pdx.cs.nb2015.reanalyzer;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Utils {
    // Java 8 has String.join() but we're using Java 7
    public static final String join(String delimiter, String[] a) {
        if (delimiter == null || a == null)
            return null;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a.length; i++) {
            sb.append(a[i]);
            if (i != a.length - 1)
                sb.append(delimiter);
        }
        return sb.toString();
    }

    public static final Map<String, String> splitQuery(String query)
            throws UnsupportedEncodingException {
        // TODO unit test
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        if (query != null) {
            String[] pairs = query.split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                query_pairs.put(
                        URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
                        URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            }
        }
        return query_pairs;
    }

    public static final boolean isZipCode(String str) {
        // TODO unit test
        if (str == null)
            return false;
        try {
            if (str.length() != 5) {
                return false;
            } else {
                Integer.parseInt(str);
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
