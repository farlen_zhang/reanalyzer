package edu.pdx.cs.nb2015.reanalyzer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// TODO use PreparedStatement to insert external values
// TODO checkstyle/findbugs
// TODO DB column types

// java -cp 'lib/*:lib/jaxrs-ri/api/*:lib/jaxrs-ri/ext/*:lib/jaxrs-ri/lib/*:bin' edu.pdx.cs.nb2015.reanalyzer.Importer 97230 97220 97202 97239

public final class Importer {

    private static Connection conn;

    public static void main(String[] zipCodes) {
        // TODO zip codes maybe well-formatted but not in OR/WA
        if (zipCodes.length < 1
                || zipCodes.length > Constants.RMLS_NUM_ZIP_CODE_LIMIT) {
            throw new IllegalArgumentException("This program must take 1-"
                    + Constants.RMLS_NUM_ZIP_CODE_LIMIT + " ZIP code(s).");
        } else {
            for (String arg : zipCodes) {
                if (!Utils.isZipCode(arg)) {
                    throw new IllegalArgumentException(
                            "All arguments need to be 5-digit ZIP codes.");
                }
            }
        }

        try {
            conn = DBManager.getConnection();
            DBManager.createTableIfNotExists(conn);

            Set<RmlsRecord> rmlsRecords = new HashSet<RmlsRecord>();
            List<String> rmlsPages = fetchRmlsData(zipCodes);
            System.out.println("Parsing RMLS pages...");
            int pageCount = 1;
            for (String rmlsPage : rmlsPages) {
                System.out.println("  Page " + pageCount++);
                rmlsRecords.addAll(parseRmlsData(rmlsPage));
            }
            System.out.println("  " + rmlsRecords.size()
                    + " address(es) successfully parsed in total.");
            pruneDBRecordsInZipCode(zipCodes);
            Map<RmlsRecord, String> deepSearchResults = getZillowDeepSearchResults(rmlsRecords);
            List<Map<String, String>> parsedZillowResults = parseZillowDeepSearchResults(deepSearchResults);
            refreshDBWithZillowData(parsedZillowResults);
            System.out.println("Done!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public static final List<String> fetchRmlsData(String[] zips)
            throws URISyntaxException, UnsupportedEncodingException {
        // TODO save MLS #
        String[] zipCodes = Arrays.copyOf(zips,
                Constants.RMLS_NUM_ZIP_CODE_LIMIT);
        System.out.println("Fetching properties in "
                + Utils.join(", ", zipCodes) + "...");
        Response resp = sendFirstRequestToRmls(zipCodes[0], zipCodes[1],
                zipCodes[2], zipCodes[3]);
        if (resp.getStatus() != 302) {
            throw new RuntimeException(
                    "First request does not return status code 302.");
        }

        Map<String, NewCookie> cookies = resp.getCookies();
        URI newLocation = new URI(Constants.RMLS_START_PAGE).resolve(resp
                .getLocation());

        List<String> rmlsPages = new LinkedList<String>();
        int pageCount = 1;
        String path = newLocation.getPath();
        Map<String, String> params = Utils.splitQuery(newLocation.getQuery());
        while (pageCount <= Constants.RMLS_PAGE_FETCH_LIMIT) {
            resp = sendOneSubsequentRequestToRmls(path, params, cookies,
                    pageCount);
            if (resp.getStatus() == 200) {
                String entity = resp.readEntity(String.class);
                // System.out.println(entity);
                rmlsPages.add(entity);
            } else {
                System.out.println("  Got status code " + resp.getStatus()
                        + " at page " + pageCount + ".  Stop here.");
                break;
            }

            pageCount++;
        }
        return rmlsPages;
    }

    /**
     * Send first request; will get a 302 response
     * 
     */
    public static final Response sendFirstRequestToRmls(String zipCode1,
            String zipCode2, String zipCode3, String zipCode4)
            throws URISyntaxException {
        System.out.println("  Sending the first request...");
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

        Form rmlsQueryForm = new Form().param("ReportID", "RC_RESULT")
                .param("SearchType", "RC")
                .param("FormID", "RC_SEARCH_RESIDENTIAL")
                .param("FormURL", "/rc2/UI/search_residential.asp")
                .param("PropType", "1")
                .param("STATE_ID", "../../RC2/UI/search_residential.asp")
                .param("PROPERTY_TYPE", "").param("PRICE", "1")
                .param("PRICE", "").param("INCLUDE_AUCTION", "3")
                .param("STATUS", "ACT").param("STATUS", "BMP")
                .param("YEAR_BUILT", "").param("YEAR_BUILT", "")
                .param("BEDROOMS", "").param("BEDROOMS", "")
                .param("BATHROOMS", "").param("BATHROOMS", "")
                .param("ZIP", zipCode1).param("ZIP", zipCode2)
                .param("ZIP", zipCode3).param("ZIP", zipCode4)
                .param("SQFT", "").param("SQFT", "").param("ACRES", "")
                .param("ACRES", "").param("SCHOOL_TYPE", "Elementary")
                .param("SCHOOL_NAME", "").param("ACCESSIBILITY_YN", "")
                .param("OHBT_DATE", "").param("COUNTY", "").param("CITY", "")
                .param("BANK_OWNED_YN", "").param("SHORT_SALE_YN", "");

        Client client = ClientBuilder.newClient(new ClientConfig());
        Builder builder = client
                .property(ClientProperties.FOLLOW_REDIRECTS, "false")
                .target(Constants.RMLS_HTTP_HOST)
                .path(new URI(Constants.RMLS_START_PAGE).resolve(
                        Constants.RMLS_START_PAGE_SUBMIT_TARGET).getPath())
                .request(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                .header("Origin", Constants.RMLS_HTTP_HOST)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Content-Length", "476");
        addHeadersToBuilder(builder);
        return builder.post(Entity.entity(rmlsQueryForm,
                MediaType.APPLICATION_FORM_URLENCODED_TYPE));
    }

    public static final Response sendOneSubsequentRequestToRmls(String path,
            Map<String, String> params, Map<String, NewCookie> cookies,
            int pageCount) throws URISyntaxException,
            UnsupportedEncodingException {
        System.out.println("  Fetching page " + pageCount + "...");
        Client client = ClientBuilder.newClient(new ClientConfig());
        WebTarget target = client.target(Constants.RMLS_HTTP_HOST).path(path);
        for (Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            if (key.equals(Constants.RMLS_PAGE_NUMBER_PARAM)) {
                target = target.queryParam(key, pageCount);
            } else {
                target = target.queryParam(key, entry.getValue());
            }
        }
        Builder builder = target.request(MediaType.TEXT_PLAIN_TYPE);
        addHeadersToBuilder(builder);
        for (NewCookie cookie : cookies.values()) {
            builder.cookie(cookie);
        }
        return builder.get();
    }

    private static final void addHeadersToBuilder(Builder builder) {
        // TODO use dedicated methods rather than header()?
        builder.header("Host", Constants.RMLS_HOST)
                .header("Accept-Encoding", "gzip, deflate")
                .header("Accept-Language",
                        "en-US,en;q=0.8,zh-CN;q=0.6,zh-TW;q=0.4")
                .header("Upgrade-Insecure-Requests", "1")
                .header("User-Agent",
                        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36")
                .header("Accept",
                        "text/html,application/xhtml+xmlpplication/xml;q=0.9,image/webp,*/*;q=0.8")
                .header("Cache-Control", "max-age=0")
                .header("Referer", Constants.RMLS_START_PAGE)
                .header("Connection", "keep-alive");
    }

    public static final Set<RmlsRecord> parseRmlsData(String rmlsPage)
            throws IOException {
        Set<RmlsRecord> result = new HashSet<RmlsRecord>();
        int successCount = 0;
        int failureCount = 0;

        Pattern linePattern = Pattern.compile("Address:");
        Pattern addrPattern = Pattern
                .compile("Address:\t+([0-9]+ .+) ([0-9]{5}).*Price:\t+?\\$((?:[0-9]|,)+)");

        BufferedReader br = new BufferedReader(new StringReader(rmlsPage));
        String line;
        while ((line = br.readLine()) != null) {
            if (linePattern.matcher(line).find()) {
                line = line.replaceAll("\\<.*?\\>", "").trim();
                Matcher m = addrPattern.matcher(line);
                if (m.find()) {
                    result.add(new RmlsRecord(m.group(1), Integer.parseInt(m
                            .group(2)), Integer.parseInt(m.group(3).replaceAll(
                            ",", ""))));
                    successCount++;
                } else {
                    System.out
                            .println("    Line contains 'Address:' but fails to parse: "
                                    + line);
                    failureCount++;
                }
            }
        }
        br.close();
        System.out.println("    Successfully parsed " + successCount
                + " address(es).  Failed to parse " + failureCount
                + " address(es).");
        return result;

    }

    private static final void pruneDBRecordsInZipCode(String[] zipCodes)
            throws SQLException {
        // TODO unit test
        Statement stmt = conn.createStatement();
        for (String zipCode : zipCodes) {
            if (zipCode != null) {
                System.out.print("Pruning zip code " + zipCode + "... ");
                System.out.print(stmt
                        .executeUpdate("delete from properties where zipcode="
                                + zipCode));
                System.out.println(" record(s) deleted.");
            }
        }
        stmt.close();
    }

    public static final Map<RmlsRecord, String> getZillowDeepSearchResults(
            Set<RmlsRecord> rmlsRecords) throws UnsupportedEncodingException {
        System.out.println("Performing Zillow Deep Search on "
                + rmlsRecords.size() + " address(es)...");
        Map<RmlsRecord, String> result = new HashMap<RmlsRecord, String>();
        if (rmlsRecords != null) {
            for (RmlsRecord rr : rmlsRecords) {
                Client client = ClientBuilder.newClient(new ClientConfig());
                String entity = client
                        .target(Constants.ZILLOW_ZWS_ENDPOINT)
                        .path("GetDeepSearchResults.htm")
                        .queryParam("zws-id", Constants.ZWS_ID)
                        .queryParam("address",
                                URLEncoder.encode(rr.streetAddr, "UTF-8"))
                        .queryParam(
                                "citystatezip",
                                URLEncoder.encode(Integer.toString(rr.zipCode),
                                        "UTF-8"))
                        .request(MediaType.TEXT_PLAIN_TYPE).get(String.class);
                result.put(rr, entity);

            }
        }
        return result;
    }

    public static final List<Map<String, String>> parseZillowDeepSearchResults(
            Map<RmlsRecord, String> deepSearchResults)
            throws ParserConfigurationException, SAXException, IOException {
        List<Map<String, String>> result = new LinkedList<Map<String, String>>();

        if (deepSearchResults != null) {
            int failureCount = 0;
            System.out.println("Parsing " + deepSearchResults.size()
                    + " Zillow Deep Search results...");
            DocumentBuilder db = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            for (Entry<RmlsRecord, String> entry : deepSearchResults.entrySet()) {
                RmlsRecord rr = entry.getKey();
                String zillowResult = entry.getValue();
                Document doc = db.parse(new InputSource(new StringReader(
                        zillowResult)));
                String errorCode = doc.getElementsByTagName("code").item(0)
                        .getFirstChild().getNodeValue();
                if (!errorCode.equals("0")) {
                    failureCount++;
                    System.out
                            .println("  Zillow Deep Search returned error code "
                                    + errorCode
                                    + ".  The address used is '"
                                    + rr + "'.");
                    continue;
                }

                HashMap<String, String> m = new HashMap<String, String>();
                for (String col : Constants.ZILLOW_DEEP_SEARCH_COLUMNS) {
                    try {
                        // do some special transformation on <region> and
                        // <zestimate>
                        if (col.startsWith("region_")) {
                            Node item = doc.getElementsByTagName("region")
                                    .item(0);
                            NamedNodeMap attributes = item.getAttributes();
                            m.put(col,
                                    attributes.getNamedItem(
                                            col.replace("region_", ""))
                                            .getNodeValue());
                        } else if (col == "zestimate") {
                            Node item = doc.getElementsByTagName("amount")
                                    .item(0);
                            Node child = item.getFirstChild();
                            m.put(col, child.getNodeValue());
                        } else {
                            Node item = doc.getElementsByTagName(col).item(0);
                            Node child = item.getFirstChild();
                            m.put(col, child.getNodeValue());
                        }
                    } catch (NullPointerException e) {
                        // if some element does not exist, continue to process
                        // the next column
                        continue;
                    }
                }
                m.put(Constants.RMLS_PRICE_COLUMN,
                        new Integer(rr.price).toString());
                // sometimes Zillow returns empty zip code... use RMLS instead
                if (m.get(Constants.ZILLOW_ZIPCODE_COLUMN) == null
                        || m.get(Constants.ZILLOW_ZIPCODE_COLUMN).trim()
                                .isEmpty()) {
                    m.put(Constants.ZILLOW_ZIPCODE_COLUMN, new Integer(
                            rr.zipCode).toString());
                }
                result.add(m);
            }
            System.out.println("  Number of Zillow Deep Search failures: "
                    + failureCount);
        }

        return result;
    }

    public static final void refreshDBWithZillowData(
            List<Map<String, String>> parsedZillowResults) throws SQLException {
        // TODO multiple values in a single insert
        // TODO timestamp column?
        // TODO unit test
        System.out.println("Refreshing DB with " + parsedZillowResults.size()
                + " records...");
        conn.setAutoCommit(false); // start a transaction
        int count = 0;
        for (Map<String, String> m : parsedZillowResults) {
            StringBuilder sb = new StringBuilder();
            sb.append("insert into properties (")
                    .append(Utils.join(", ",
                            Constants.ZILLOW_DEEP_SEARCH_COLUMNS)).append(", ")
                    .append(Constants.RMLS_PRICE_COLUMN).append(") values (");
            for (int i = 0; i < Constants.ZILLOW_DEEP_SEARCH_COLUMNS.length; i++) {
                String value = m.get(Constants.ZILLOW_DEEP_SEARCH_COLUMNS[i]);
                if (value == null) {
                    sb.append("null");
                } else {
                    sb.append("'").append(value).append("'");
                }
                sb.append(", ");
            }
            sb.append("'").append(m.get(Constants.RMLS_PRICE_COLUMN))
                    .append("')");
            Statement stmt = conn.createStatement();
            try {
                count += stmt.executeUpdate(sb.toString());
            } catch (SQLException e) {
                System.out
                        .println("  An error occured when inserting this record: "
                                + m);
            } finally {
                stmt.close();
            }
        }
        conn.commit();
        System.out.println("  " + count + " rows inserted.");
    }
}
