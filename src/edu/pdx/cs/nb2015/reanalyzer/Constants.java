package edu.pdx.cs.nb2015.reanalyzer;

public final class Constants {
    public static final String DB_FILE = "jdbc:sqlite:REAnalyzer.db";

    public static final int RMLS_NUM_ZIP_CODE_LIMIT = 4;
    public static final String RMLS_HOST = "www.rmls.com";
    public static final String RMLS_HTTP_HOST = "http://www.rmls.com";
    public static final String RMLS_START_PAGE = "http://www.rmls.com/rc2/UI/search_residential.asp";
    public static final String RMLS_START_PAGE_SUBMIT_TARGET = "../engine/sqlGenerator_RmlsCom.asp";
    public static final int RMLS_PAGE_FETCH_LIMIT = 100;
    public static final String RMLS_PAGE_NUMBER_PARAM = "P";

    public static final String ZWS_ID = "X1-ZWz19zg92b8sgb_avwne";
    public static final String ZILLOW_ZWS_ENDPOINT = "http://www.zillow.com/webservice/";
    public static final String ZILLOW_ZIPCODE_COLUMN = "zipcode";
    public static final String[] ZILLOW_DEEP_SEARCH_COLUMNS = { "zpid",
            "homedetails", "mapthishome", "comparables", "street",
            ZILLOW_ZIPCODE_COLUMN, "city", "state", "latitude", "longitude",
            "FIPScounty", "useCode", "yearBuilt", "lotSizeSqFt",
            "finishedSqFt", "bathrooms", "bedrooms", "lastSoldDate",
            "zestimate", "region_name", "region_id", "region_type" };

    public static final String RMLS_PRICE_COLUMN = "rmls_price";
    public static final String PRIMARY_KEY_COLUMNS = "street, zipcode";

    public static final String UI_BEST_DEALS_LIMIT = "5";
};
