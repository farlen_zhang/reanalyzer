package edu.pdx.cs.nb2015.reanalyzer;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class RmlsRecord {
    final String streetAddr;
    final int zipCode;
    final int price;

    public RmlsRecord(String sa, int zip, int prc) {
        streetAddr = sa;
        zipCode = zip;
        price = prc;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof RmlsRecord))
            return false;
        if (obj == this)
            return true;

        RmlsRecord rhs = (RmlsRecord) obj;
        return new EqualsBuilder().append(streetAddr.toLowerCase(),
                rhs.streetAddr.toLowerCase())
                .append(zipCode, rhs.zipCode).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(streetAddr.toLowerCase())
                .append(zipCode).toHashCode();
    }

    @Override
    public String toString() {
        return streetAddr + "|" + zipCode + "|" + price;
    }
}
