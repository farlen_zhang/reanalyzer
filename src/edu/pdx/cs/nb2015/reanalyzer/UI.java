package edu.pdx.cs.nb2015.reanalyzer;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

// TODO wrap outputText in a scroll pane
// TODO clicking on a link may throw a java.lang.UnsupportedOperationException

public final class UI extends JFrame {

    private static final long serialVersionUID = 1L;
    private JTextField[] textFields;
    private JTextPane outputText;

    public static void main(String[] args) {
        new UI("Real Property Analyzer");
    }

    private UI(String string) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel controlPanel = createControlPanel();
        outputText = createOutputText();

        getContentPane().add(
                new JSplitPane(JSplitPane.VERTICAL_SPLIT, controlPanel,
                        outputText));

        pack();
        setVisible(true);
    }

    private JPanel createControlPanel() {
        JPanel controlPanel = new JPanel(new FlowLayout());
        createZipCodeFields(controlPanel);
        createButtons(controlPanel);
        return controlPanel;
    }

    private JTextPane createOutputText() {
        JTextPane outputText = new JTextPane();
        outputText.setEnabled(true);
        outputText.setEditable(false);
        outputText.setFocusable(true);
        outputText.setContentType("text/html");
        outputText.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(e.getURL().toURI());
                        } catch (IOException | URISyntaxException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });
        return outputText;
    }

    private void createZipCodeFields(JPanel controlPanel) {
        Border border = BorderFactory.createLineBorder(Color.BLUE, 1);
        List<JTextField> lst = new LinkedList<JTextField>();
        for (int i = 0; i < Constants.RMLS_NUM_ZIP_CODE_LIMIT; i++) {
            JTextField tf = new JTextField(10);
            tf.setBorder(border);
            controlPanel.add(tf);
            lst.add(tf);
        }
        textFields = lst.toArray(new JTextField[0]);
    }

    private void createButtons(JPanel controlPanel) {
        JButton fetchButton = new JButton("Fetch data");
        controlPanel.add(fetchButton);
        fetchButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> zipCodes = getZipCodes();
                if (zipCodes.isEmpty()) {
                    outputText.setText("Need zip codes.");
                    return;
                }
                try {
                    Importer.main(zipCodes.toArray(new String[0]));
                } catch (IllegalArgumentException ex) {
                    outputText.setText(ex.getMessage());
                }
                outputText.setText("Done.");
            }

        });

        JButton showButton = new JButton("Show best deals!");
        controlPanel.add(showButton);
        showButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                outputText.setText("");
                Connection conn = null;
                List<String> zipCodes = getZipCodes();
                if (zipCodes.isEmpty()) {
                    outputText.setText("Need zip codes.");
                    return;
                }
                try {
                    conn = DBManager.getConnection();
                    StringBuffer sb = new StringBuffer();
                    sb.append("<html>\n");
                    for (String zipCode : zipCodes) {
                        StringBuffer sql = new StringBuffer();
                        sql.append("select street, rmls_price, zestimate, homedetails from properties where zipcode='");
                        sql.append(zipCode);
                        sql.append("' order by (cast(zestimate as float) - cast(rmls_price as float)) / cast(rmls_price as float) desc limit ");
                        sql.append(Constants.UI_BEST_DEALS_LIMIT);

                        Statement stmt = conn.createStatement();
                        ResultSet rs = stmt.executeQuery(sql.toString());
                        sb.append("<table border='1'>\n  <tr><th>Zip Code ");
                        sb.append(zipCode);
                        sb.append("</th><th>Street</th><th>RMLS Price</th><th>Zestimate</th><th>Zillow link</th></tr>\n");
                        while (rs.next()) {
                            sb.append("  <tr><td>");
                            sb.append(zipCode);
                            sb.append("</td><td>");
                            sb.append(rs.getString("street"));
                            sb.append("</td><td>$");
                            sb.append(rs.getInt("rmls_price"));
                            sb.append("</td>");
                            sb.append("<td>$");
                            sb.append(rs.getInt("zestimate"));
                            sb.append("</td>");
                            sb.append("<td>");
                            sb.append("<a href='");
                            sb.append(rs.getString("homedetails"));
                            sb.append("''>Click here</a>");
                            sb.append("</td></tr>\n");
                        }
                        stmt.close();
                        sb.append("</table>\n<hr>\n");
                    }
                    sb.append("</html>");
                    // System.out.println(sb.toString());
                    outputText.setText(sb.toString());
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    try {
                        if (conn != null)
                            conn.close();
                    } catch (SQLException ex) {
                        System.err.println(ex);
                    }
                }
            }
        });
    }

    private List<String> getZipCodes() {
        List<String> zipCodes = new LinkedList<String>();
        for (JTextField tf : textFields) {
            String text = tf.getText();
            if (Utils.isZipCode(text))
                zipCodes.add(text);
        }
        return zipCodes;
    }
}
