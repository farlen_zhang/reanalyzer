package edu.pdx.cs.nb2015.reanalyzer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public final class DBManager {

    public static void main(String[] args) throws ClassNotFoundException {
        Connection conn = null;
        try {
            conn = getConnection();
            dropTable(conn);
            createTableIfNotExists(conn);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public static final void createTableIfNotExists(Connection conn)
            throws ClassNotFoundException, SQLException {
        StringBuilder sb = new StringBuilder(
                "create table if not exists properties (")
                .append(Constants.RMLS_PRICE_COLUMN)
                .append(" text, ")
                .append(Utils.join(" text, ",
                        Constants.ZILLOW_DEEP_SEARCH_COLUMNS))
                .append(" text, primary key (")
                .append(Constants.PRIMARY_KEY_COLUMNS).append("))");
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(sb.toString());
        stmt.close();
    }

    public static final void dropTable(Connection conn)
            throws ClassNotFoundException, SQLException {
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("drop table if exists properties");
        stmt.close();
    }

    public static final Connection getConnection()
            throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection(Constants.DB_FILE);
        return connection;
    }
}
